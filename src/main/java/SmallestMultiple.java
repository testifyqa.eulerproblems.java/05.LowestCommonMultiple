import org.junit.Assert;

public class SmallestMultiple {

    public static void main(String[] args) {
        tests(1, 10, 2520);
        tests(11, 20, 232792560);
    }

    private static int smallestMultiple(int minNum, int maxNum) {
        int lowestCommonMultiple = 2520;

        for (int i=minNum; i<=maxNum; i++) {
            if (lowestCommonMultiple % i != 0) {
                i = minNum-1;
                lowestCommonMultiple++;
            }
        }
        System.out.println("\nThe LCM for 1-" + maxNum + " is: " + lowestCommonMultiple);
        return lowestCommonMultiple;
    }

    private static void tests(int minNum, int maxNum, int lowestCommonMultiple) {
        Assert.assertEquals(smallestMultiple(minNum, maxNum), lowestCommonMultiple);
    }
}
